# Apa itu Semantic Web?

> The Semantic Web is not a separate Web but an extension of the current one, in which information is given well-defined meaning, better enabling computers and people to work in cooperation. The first steps in weaving the Semantic Web into the structure of the existing Web are already under way. In the near future, these developments will usher in significant new functionality as machines become much better able to process and "understand" the data that they merely display at present.” (Tim Berners-Lee, James Hendler, Ora Lassila, Scientific American, May 2001)

Website pertama kali dikembangkan di CERN oleh Tim Berners-Lee. Saat itu, banyak ...

Pengertian website berkembang, dari Web 1.0 yang hanya sebatas dokumen-dokumen biasa yang dimuat ke jejaring internet, atau istilahnya read-only web. Kemudian berkembang Web 2.0, di mana kita sebagai individu dapat memasukkan konten ke dalam website tersebut, seperti melakukan chatting, berkomentar di halaman website. Kemudian, berkembang Web 3.0, atau web yang pintar, intelligent web, di mana seharusnya mesin dapat memahami konten dari website yang dimuat. 